class WelcomeController < ApplicationController

	def index
  	
		require 'rubygems'
		require 'nokogiri'
		require 'open-uri'
		require 'mechanize'

		page_url = "https://ru.investing.com/technical/%D0%98%D0%BD%D0%B4%D0%B8%D0%BA%D0%B0%D1%82%D0%BE%D1%80%D1%8B"
		markup_link = 'html body div section#leftColumn form table#curr_table.clear.genTbl.closedTbl.movingAveragesTbl.arial_12.bold'

		agent = Mechanize.new
		page = agent.get(page_url)

		form = page.form_with(name: 'toolTable')
		field_period = form.field_with(name: 'period')

		period_hash = {}
		currency_hash_1 = {}

		[300, 900, 1800, 3600, 18000].each do |period|

		  field_period.value = period
		  page = agent.submit form

		  currency_hash_1 = {}
		  currency_hash_2 = {}
		  currency_hash = {}

		  flag = 0
		  page.css("h2").each do |currency|
		            
		    currency_hash_1[flag] = currency.text
		    flag += 1

		  end

		  flag = 0
		  page.css(markup_link).each do |element|

		    str = element.text

		    str.insert(str.index('Продавать'), '\n')
		    str.insert(str.index('Нейтрально'), '\n')
		    str.insert(str.index('Сводные'), '\n')
		    arr = str.split('\n')

		    element_hash = {}

		    arr.each do |num|

		      status = num.strip.split(':')
		      element_hash[status.first.to_sym] = status.last.to_s

		    end

		    currency_hash_2[flag] = element_hash
		    flag += 1

		  end

		  (0..7).each do |count|

		    currency_hash[currency_hash_1[count]] = currency_hash_2[count]

		  end

		  period_hash[page.css("select").css("option[selected]").text.to_sym] = currency_hash

		end

		@cur_hash = {}
		time_hash = {}

		currency_hash_1.each do |index, currency|

			time_hash = {}

			period_hash.each do |key, value|

		    	value.each do |cur, stats|  

		    		time_hash[key] = stats if cur == currency

		    	end

		  	end

		  	@cur_hash[currency] = time_hash

		end

	end

end